#![feature(type_alias_impl_trait, const_trait_impl, effects)]
#![no_std]
#![no_main]
//#![forbid(unsafe_code)]

mod bootload;
mod hid;
mod i2c;
mod layout;
mod leds;
mod matrix;
mod setup;
mod util;

use panic_halt as _;

mod config {
    pub const COLS: usize = 7;
    pub const ROWS: usize = 6;
    pub const FULL_COLS: usize = COLS * 2;
}

#[rtic::app(device = ::hal::pac, peripherals = true, dispatchers = [TIM7, TIM4])]
mod app {
    use crate::{bootload, i2c::RightI2c, layout, leds::*, matrix::Matrix, util::serial_msg};
    use core::mem::MaybeUninit;
    use hal::{prelude::*, usb::UsbBusType};
    use rtic_monotonics::systick::{ExtU32, Systick};
    use usb_device::{class_prelude::*, prelude::*};

    #[shared]
    struct Shared {
        usb_hid_class: crate::hid::HidClass,
        usb_serial_class: usbd_serial::SerialPort<'static, UsbBusType>,
        #[lock_free]
        usb_device: UsbDevice<'static, UsbBusType>,
    }

    #[local]
    struct Local {
        layout: layout::MyLayout,
        leds: Leds,
        i2c: RightI2c,
        matrix: Matrix,
    }

    #[cortex_m_rt::pre_init]
    unsafe fn before_init() {
        bootload::check();
    }

    #[init(local = [
        usb_bus: MaybeUninit<UsbBusAllocator<UsbBusType>> = MaybeUninit::uninit(),
    ])]
    fn init(cx: init::Context) -> (Shared, Local) {
        const SYSCLK: u32 = 48_000_000;

        const USB_VID_PID: UsbVidPid = UsbVidPid(0x3297, 0x1969);
        //const USB_VID_PID: UsbVidPid = UsbVidPid(0x05AC, 0x1969); Apple
        const MANUFACTURER: &str = "ZSA Technology Labs";
        const PRODUCT: &str = "Moonlander Mark I";
        const SERIAL: &str = "fefe9bda-903a-11ed-a1eb-0242ac120002";

        let mut rcc = cx.device.RCC.constrain();
        let mut flash = cx.device.FLASH.constrain();

        let systick_mono_token = rtic_monotonics::create_systick_token!();
        Systick::start(cx.core.SYST, SYSCLK, systick_mono_token);

        let clocks = rcc
            .cfgr
            .use_hse(8.MHz()) // High Speed External clock signal
            .sysclk(SYSCLK.Hz().into())
            .pclk1(24.MHz())
            .pclk2(24.MHz())
            .freeze(&mut flash.acr);
        assert!(clocks.usbclk_valid());

        let mut gpioa = cx.device.GPIOA.split(&mut rcc.ahb);
        let mut gpiob = cx.device.GPIOB.split(&mut rcc.ahb);

        /* CHANNELS */

        /* SETUP */

        let mut leds = Leds::new(
            gpiob.pb3,
            gpiob.pb4,
            gpiob.pb5,
            &mut gpiob.moder,
            &mut gpiob.otyper,
        );
        // By default the green led remains on. I turn everything off.
        leds.set(LedsEvent {
            left_blue: Off,
            left_green: Off,
            left_red: Off,
            right_blue: Off,
            right_green: Off,
            right_red: Off,
        });

        let usb_bus = crate::setup::usb(
            cx.local.usb_bus,
            cx.device.USB,
            gpioa.pa11,
            gpioa.pa12,
            &mut gpioa.moder,
            &mut gpioa.otyper,
            &mut gpioa.afrh,
        );

        let usb_hid_class = crate::hid::HidClass::new(
            crate::hid::Keyboard::default(),
            usb_bus,
            crate::hid::HidProtocol::Keyboard,
        );
        let usb_serial_class = usbd_serial::SerialPort::new(&usb_bus);

        let usb_device = UsbDeviceBuilder::new(usb_bus, USB_VID_PID)
            .manufacturer(MANUFACTURER)
            .product(PRODUCT)
            .serial_number(SERIAL)
            .device_class(usbd_serial::USB_CLASS_CDC)
            .build();

        let mut matrix = Matrix::new(
            &mut gpioa.moder,
            &mut gpiob.moder,
            &mut gpioa.otyper,
            &mut gpiob.otyper,
            &mut gpiob.pupdr,
            gpioa.pa0,
            gpioa.pa1,
            gpioa.pa2,
            gpioa.pa3,
            gpioa.pa6,
            gpioa.pa7,
            gpiob.pb0,
            gpiob.pb10,
            gpiob.pb11,
            gpiob.pb12,
            gpiob.pb13,
            gpiob.pb14,
            gpiob.pb15,
        );
        matrix.reset();

        // I2C
        // Sets the led to on then off, to show that initialization succeeded:
        let i2c = crate::i2c::RightI2c::new(
            cx.device.I2C1,
            (gpiob.pb6, gpiob.pb7),
            (
                &mut gpiob.moder,
                &mut gpiob.otyper,
                &mut gpiob.afrl,
                &mut gpiob.pupdr,
            ),
            clocks,
            &mut rcc.apb1,
        );

        /* INITIALIZATION */

        let layout = match layout::create() {
            Ok(layout) => layout,
            Err(_e) => {
                leds.set(LedsEvent {
                    left_red: On,
                    ..Default::default()
                });
                loop {}
            }
        };

        matrix_scan::spawn().unwrap();
        serial_msg!("Initialized");

        (
            Shared {
                usb_device,
                usb_hid_class,
                usb_serial_class,
            },
            Local {
                leds,
                i2c,
                layout,
                matrix,
            },
        )
    }

    #[task(binds = USB_LP_CAN_RX0, shared = [usb_device, usb_hid_class, usb_serial_class])]
    fn usb_poll(cx: usb_poll::Context) {
        (cx.shared.usb_hid_class, cx.shared.usb_serial_class)
            .lock(|hid, serial| cx.shared.usb_device.poll(&mut [hid, serial]));
    }

    #[task(local = [i2c, matrix, layout], shared = [usb_hid_class])]
    async fn matrix_scan(cx: matrix_scan::Context) {
        let mut usb_class = cx.shared.usb_hid_class;

        // Wait 1 second before scanning the matrix:
        Systick::delay(1.secs()).await;

        loop {
            for report in cx.local.layout.reports(cx.local.matrix.scan(cx.local.i2c)) {
                if usb_class.lock(|class| class.device.set_report_and_has_change(report)) {
                    while let Ok(0) =
                        usb_class.lock(|class| class.write_report(&report.into_bytes()))
                    {
                        Systick::delay(10.micros()).await;
                    }
                }
            }
            Systick::delay(1.millis()).await;
        }
    }

    #[task(shared = [usb_serial_class])]
    async fn send_debug_message(mut cx: send_debug_message::Context, msg: heapless::Vec<u8, 255>) {
        let _ = cx.shared.usb_serial_class.lock(|class| class.write(&msg));
    }

    #[task(local = [leds])]
    async fn leds_events(cx: leds_events::Context, state: LedsEvent) {
        cx.local.leds.set(state);
    }
}

pub struct MyMonotonicTimer;

impl keebit::prelude::MonotonicTimer for MyMonotonicTimer {
    fn now(&mut self) -> u64 {
        use rtic_monotonics::Monotonic as _;
        rtic_monotonics::systick::Systick::now().ticks().into()
    }
}
