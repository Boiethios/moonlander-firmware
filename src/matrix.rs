use crate::{
    config::{FULL_COLS, ROWS},
    i2c::RightI2c,
    util::ResultExt as _,
};
use hal::{
    gpio::*,
    hal::digital::v2::{InputPin, OutputPin},
};

/// Double downgrade.
macro_rules! dd {
    ( $pin:expr ) => {
        $pin.downgrade().downgrade()
    };
}

pub struct Matrix {
    // Output:
    cols: [PXx<Output<PushPull>>; crate::config::COLS],
    // Input:
    rows: [PXx<Input>; crate::config::ROWS],
}

impl Matrix {
    pub fn new(
        modera: &mut gpioa::MODER,
        moderb: &mut gpiob::MODER,
        otypera: &mut gpioa::OTYPER,
        otyperb: &mut gpiob::OTYPER,
        pupdrb: &mut gpiob::PUPDR,
        pa0: Pin<Gpioa, U<0>, Input>,
        pa1: Pin<Gpioa, U<1>, Input>,
        pa2: Pin<Gpioa, U<2>, Input>,
        pa3: Pin<Gpioa, U<3>, Input>,
        pa6: Pin<Gpioa, U<6>, Input>,
        pa7: Pin<Gpioa, U<7>, Input>,
        pb0: Pin<Gpiob, U<0>, Input>,
        pb10: Pin<Gpiob, U<10>, Input>,
        pb11: Pin<Gpiob, U<11>, Input>,
        pb12: Pin<Gpiob, U<12>, Input>,
        pb13: Pin<Gpiob, U<13>, Input>,
        pb14: Pin<Gpiob, U<14>, Input>,
        pb15: Pin<Gpiob, U<15>, Input>,
    ) -> Self {
        Matrix {
            // Output:
            cols: [
                dd!(pa0.into_push_pull_output(modera, otypera)),
                dd!(pa1.into_push_pull_output(modera, otypera)),
                dd!(pa2.into_push_pull_output(modera, otypera)),
                dd!(pa3.into_push_pull_output(modera, otypera)),
                dd!(pa6.into_push_pull_output(modera, otypera)),
                dd!(pa7.into_push_pull_output(modera, otypera)),
                dd!(pb0.into_push_pull_output(moderb, otyperb)),
            ],
            // Input:
            rows: [
                dd!(pb10.into_pull_up_input(moderb, pupdrb)),
                dd!(pb11.into_pull_up_input(moderb, pupdrb)),
                dd!(pb12.into_pull_up_input(moderb, pupdrb)),
                dd!(pb13.into_pull_up_input(moderb, pupdrb)),
                dd!(pb14.into_pull_up_input(moderb, pupdrb)),
                dd!(pb15.into_pull_up_input(moderb, pupdrb)),
            ],
        }
    }

    pub fn reset(&mut self) {
        for pin in &mut self.cols {
            pin.set_high().into_ok_();
        }
    }

    pub fn scan(&mut self, i2c: &mut RightI2c) -> [[bool; FULL_COLS]; ROWS] {
        let mut state = [[false; FULL_COLS]; ROWS];
        let mut i2c_scanner = i2c.scanner();

        for (j, col_pin) in self.cols.iter_mut().enumerate() {
            col_pin.set_low().into_ok_(); // The pin is ON

            // Slave write:
            i2c_scanner.write(j);

            // Master read:
            for (i, row_pin) in self.rows.iter().enumerate() {
                state[i][j] = row_pin.is_low().into_ok_();
            }

            // Slave read:
            for (i, active) in i2c_scanner.read().into_iter().enumerate() {
                state[i][FULL_COLS - j - 1] = active;
            }

            col_pin.set_high().into_ok_(); // The pin is OFF
        }

        state
    }
}
