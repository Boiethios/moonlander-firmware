//! The USB Human Interface Devices is a protocol allowing to communicate
//! with the OS as a keyboard (and others): https://wiki.osdev.org/USB_Human_Interface_Devices
//!
//! Specs:
//! - https://usb.org/sites/default/files/hid1_11.pdf
//! - https://usb.org/sites/default/files/hut1_22.pdf

/*
 * Order of a USB configuration:
 * - Device
 * - Configuration (e.g. different power requirement)
 * - Interface (different functions. e.g. scanner/printer)
 * - Endpoint/index
 */

mod class;
mod device;

pub use class::*;
pub use device::*;

#[derive(Debug, Clone, Copy)]
pub struct DescriptorConfig {
    /// bcdHID in the spec.
    specification_release: u16,
    /// bCountryCode in the spec.
    country_code: CountryCode,
    /// bNumDescriptors in the spec.
    num_descriptors: u8,
    /// bDescriptorType in the spec.
    /// Section 7.1.2: Set_Descriptor Request for a table of class descriptor constants.
    descriptor_type: DescriptorType,
    /// wDescriptorLength in the spec.
    descriptor_len: u16,
}

impl DescriptorConfig {
    pub fn new(descriptor_len: usize) -> Self {
        DescriptorConfig {
            specification_release: 0x111,
            country_code: CountryCode::NotSupported,
            num_descriptors: 1,
            descriptor_type: DescriptorType::Report,
            descriptor_len: descriptor_len.try_into().expect("Descriptor is too long"),
        }
    }

    pub fn into_bytes(self) -> [u8; 7] {
        let specification_release = self.specification_release.to_le_bytes();
        let descriptor_len = self.descriptor_len.to_le_bytes();

        [
            specification_release[0],
            specification_release[1],
            self.country_code as u8,
            self.num_descriptors,
            self.descriptor_type as u8,
            descriptor_len[0],
            descriptor_len[1],
        ]
    }
}

#[allow(dead_code)]
#[derive(Clone, Copy, Debug, PartialEq)]
#[repr(u8)]
pub enum CountryCode {
    NotSupported = 00,
    Arabic = 01,
    Belgian = 02,
    CanadianBilingual = 03,
    CanadiaFrench = 04,
    CzechRepublic = 05,
    Danish = 06,
    Finnish = 07,
    French = 08,
    German = 09,
    Greek = 10,
    Hebrew = 11,
    Hungary = 12,
    InternationalIso = 13,
    Italian = 14,
    JapanKatakana = 15,
    Korean = 16,
    LatinAmerican = 17,
    NetherlandsDutch = 18,
    Norwegian = 19,
    PersianFarsi = 20,
    Poland = 21,
    Portuguese = 22,
    Russia = 23,
    Slovakia = 24,
    Spanish = 25,
    Swedish = 26,
    SwissFrench = 27,
    SwissGerman = 28,
    Switzerland = 29,
    Taiwan = 30,
    TurkishQ = 31,
    Uk = 32,
    Us = 33,
    Yugoslavia = 34,
    TurkishF = 35,
}

#[allow(dead_code)]
#[derive(Debug, Clone, Copy)]
#[repr(u8)]
pub enum DescriptorType {
    Hid = 0x21,
    Report = 0x22,
    Physical = 0x23,
}
