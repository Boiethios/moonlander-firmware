use core::sync::atomic::{AtomicBool, Ordering::Relaxed};

use crate::util::ResultExt;
use hal::{
    gpio::{gpiob, Alternate, Gpiob, Input, Output, Pin, PushPull, U},
    hal::digital::v2::OutputPin,
};

#[derive(Default)]
pub struct LedsEvent {
    pub left_blue: LedState,
    pub left_green: LedState,
    pub left_red: LedState,
    pub right_blue: LedState,
    pub right_green: LedState,
    pub right_red: LedState,
}

pub use LedState::*;
#[derive(Default)]
pub enum LedState {
    /// Turns the led on.
    On,
    /// Turns the led off.
    Off,
    /// Do not change the led state.
    #[default]
    Nil,
}

static RIGHT_BLUE: AtomicBool = AtomicBool::new(false);
static RIGHT_GREEN: AtomicBool = AtomicBool::new(false);
static RIGHT_RED: AtomicBool = AtomicBool::new(false);

pub struct Leds {
    pub red: hal::gpio::Pin<Gpiob, U<3>, Output<PushPull>>,
    pub green: hal::gpio::Pin<Gpiob, U<4>, Output<PushPull>>,
    pub blue: hal::gpio::Pin<Gpiob, U<5>, Output<PushPull>>,
}

impl Leds {
    pub fn new(
        pb3: Pin<Gpiob, U<3>, Alternate<PushPull, 0>>,
        pb4: Pin<Gpiob, U<4>, Alternate<PushPull, 0>>,
        pb5: Pin<Gpiob, U<5>, Input>,
        moder: &mut gpiob::MODER,
        otyper: &mut gpiob::OTYPER,
    ) -> Self {
        Self {
            red: pb3.into_push_pull_output(moder, otyper),
            green: pb4.into_push_pull_output(moder, otyper),
            blue: pb5.into_push_pull_output(moder, otyper),
        }
    }

    pub fn set(&mut self, state: LedsEvent) {
        fn set_left_pin(
            pin: &mut dyn OutputPin<Error = core::convert::Infallible>,
            state: LedState,
        ) {
            match state {
                On => pin.set_high().into_ok_(),
                Off => pin.set_low().into_ok_(),
                Nil => (),
            }
        }

        fn set_right_led(led: &AtomicBool, state: LedState) {
            match state {
                On => led.store(true, Relaxed),
                Off => led.store(false, Relaxed),
                Nil => (),
            }
        }

        set_left_pin(&mut self.blue, state.left_blue);
        set_left_pin(&mut self.green, state.left_green);
        set_left_pin(&mut self.red, state.left_red);

        set_right_led(&RIGHT_BLUE, state.right_blue);
        set_right_led(&RIGHT_GREEN, state.right_green);
        set_right_led(&RIGHT_RED, state.right_red);
    }

    // Blue - Green - Red
    pub fn right() -> (bool, bool, bool) {
        (
            RIGHT_BLUE.load(Relaxed),
            RIGHT_GREEN.load(Relaxed),
            RIGHT_RED.load(Relaxed),
        )
    }

    pub fn spawn_event(event: LedsEvent) {
        crate::app::leds_events::spawn(event).ok();
    }
}

impl From<bool> for LedState {
    fn from(value: bool) -> Self {
        if value {
            On
        } else {
            Off
        }
    }
}

impl LedsEvent {
    pub fn code_left(code: u8) -> Self {
        match code {
            0 => LedsEvent {
                left_blue: Off,
                left_green: Off,
                left_red: Off,
                ..Default::default()
            },
            1 => LedsEvent {
                left_blue: Off,
                left_green: Off,
                left_red: On,
                ..Default::default()
            },
            2 => LedsEvent {
                left_blue: Off,
                left_green: On,
                left_red: Off,
                ..Default::default()
            },
            3 => LedsEvent {
                left_blue: Off,
                left_green: On,
                left_red: On,
                ..Default::default()
            },
            4 => LedsEvent {
                left_blue: On,
                left_green: Off,
                left_red: Off,
                ..Default::default()
            },
            5 => LedsEvent {
                left_blue: On,
                left_green: Off,
                left_red: On,
                ..Default::default()
            },
            6 => LedsEvent {
                left_blue: On,
                left_green: On,
                left_red: Off,
                ..Default::default()
            },
            _ => LedsEvent {
                left_blue: On,
                left_green: On,
                left_red: On,
                ..Default::default()
            },
        }
    }

    pub fn code_right(code: u8) -> Self {
        match code {
            0 => LedsEvent {
                right_blue: Off,
                right_green: Off,
                right_red: Off,
                ..Default::default()
            },
            1 => LedsEvent {
                right_blue: Off,
                right_green: Off,
                right_red: On,
                ..Default::default()
            },
            2 => LedsEvent {
                right_blue: Off,
                right_green: On,
                right_red: Off,
                ..Default::default()
            },
            3 => LedsEvent {
                right_blue: Off,
                right_green: On,
                right_red: On,
                ..Default::default()
            },
            4 => LedsEvent {
                right_blue: On,
                right_green: Off,
                right_red: Off,
                ..Default::default()
            },
            5 => LedsEvent {
                right_blue: On,
                right_green: Off,
                right_red: On,
                ..Default::default()
            },
            6 => LedsEvent {
                right_blue: On,
                right_green: On,
                right_red: Off,
                ..Default::default()
            },
            _ => LedsEvent {
                right_blue: On,
                right_green: On,
                right_red: On,
                ..Default::default()
            },
        }
    }
}
