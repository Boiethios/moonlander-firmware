use super::{DescriptorConfig, Keyboard};
use crate::util::serial_msg;
use hal::usb::UsbBusType;
use usb_device::{
    class_prelude::*,
    control::{Recipient, Request, RequestType},
    Result as UsbResult,
};

pub struct HidClass {
    pub device: Keyboard,
    interface: InterfaceNumber,
    endpoint_interrupt_in: EndpointIn<'static, UsbBusType>,
    subclass: Subclass,
    protocol: HidProtocol,
}

impl HidClass {
    pub fn new(
        hid_device: Keyboard,
        alloc: &'static UsbBusAllocator<UsbBusType>,
        protocol: HidProtocol,
    ) -> Self {
        let max_packet_size = hid_device.max_packet_size();

        HidClass {
            device: hid_device,
            interface: alloc.interface(),
            endpoint_interrupt_in: alloc.interrupt(max_packet_size, 10),
            subclass: Subclass::BootInterface,
            protocol,
        }
    }

    pub fn write_report(&mut self, report: &[u8]) -> Result<usize, ()> {
        match self.endpoint_interrupt_in.write(report) {
            Ok(count) => Ok(count),
            Err(UsbError::WouldBlock) => Ok(0),
            Err(_) => Err(()),
        }
    }

    pub fn is_my_interface(&self, index_from_request: u16) -> bool {
        index_from_request == u8::from(self.interface).into()
    }
}

impl UsbClass<UsbBusType> for HidClass {
    fn get_configuration_descriptors(&self, writer: &mut DescriptorWriter) -> UsbResult<()> {
        writer.interface(
            self.interface,
            INTERFACE_CLASS_HID,
            self.subclass as u8,
            self.protocol as u8,
        )?;

        let dconfig = DescriptorConfig::new(self.device.report_descriptor().len());

        writer.write(DescriptorType::Hid as u8, &dconfig.into_bytes())?;
        writer.endpoint(&self.endpoint_interrupt_in)
    }

    fn get_bos_descriptors(&self, _writer: &mut BosWriter) -> UsbResult<()> {
        Ok(())
    }

    fn get_string(&self, _index: StringIndex, _lang_id: u16) -> Option<&str> {
        None
    }

    fn reset(&mut self) {}

    fn poll(&mut self) {}

    // Control request from the host to the device.
    fn control_out(&mut self, xfer: ControlOut<UsbBusType>) {
        let req = xfer.request();

        match req {
            // https://www.usb.org/sites/default/files/hid1_11.pdf p.62
            Request {
                request_type: RequestType::Class,
                recipient: Recipient::Interface,
                request: class_request::SET_REPORT,
                index: interface_number,
                value,
                ..
            } if self.is_my_interface(*interface_number) => {
                let [report_type, _report_id] = value.to_be_bytes();
                self.device.read_report_from_host(xfer, report_type);
            }

            // https://www.usb.org/sites/default/files/hid1_11.pdf p.62
            Request {
                request_type: RequestType::Class,
                recipient: Recipient::Interface,
                request: class_request::SET_IDLE,
                index: interface_number,
                value,
                ..
            } if self.is_my_interface(*interface_number) => {
                let [duration, _report_id] = value.to_be_bytes();
                self.device.set_idle_for_duration(xfer, duration);
            }

            // https://www.usb.org/sites/default/files/hid1_11.pdf p.64
            Request {
                request_type: RequestType::Class,
                recipient: Recipient::Interface,
                request: class_request::SET_PROTOCOL,
                index: interface_number,
                value,
                ..
            } if self.is_my_interface(*interface_number) => {
                let [_zero, protocol] = value.to_be_bytes();
                self.device.set_protocol(xfer, protocol);
            }

            // We don't care about any other case (it's not in the HID standard)
            _ => {
                //Leds::spawn_event(LedsEvent {
                //    left_red: true.into(),
                //    ..Default::default()
                //});
                //xfer.reject().ok();
            }
        }
    }

    // Control request from the device to the host.
    fn control_in(&mut self, xfer: ControlIn<UsbBusType>) {
        let req = xfer.request();

        match req {
            // This is the only “in” standard request:
            // https://www.usb.org/sites/default/files/hid1_11.pdf p.59
            Request {
                request_type: RequestType::Standard,
                recipient: Recipient::Interface,
                request: Request::GET_DESCRIPTOR,
                // If a HID class descriptor is being requested then the wIndex field indicates
                // the number of the HID Interface.
                index: interface_number,
                ..
            } if self.is_my_interface(*interface_number) => {
                let (descriptor_type, _descriptor_index) = req.descriptor_type_index();
                // // The low byte is the Descriptor Index used to specify the set for Physical
                // // Descriptors, and is reset to zero for other HID class descriptors.
                // assert_eq!(
                //     descriptor_index, 0,
                //     "for a HID device, the descriptor index is 0"
                // );
                if descriptor_type == DescriptorType::Report {
                    self.device.write_report_descriptor_to_host(xfer);
                }
            }

            // https://www.usb.org/sites/default/files/hid1_11.pdf p.61
            Request {
                request_type: RequestType::Class,
                //recipient: Recipient::Interface,
                request: class_request::GET_REPORT,
                index: interface_number,
                value,
                ..
            } if self.is_my_interface(*interface_number) => {
                let [report_type, _report_id] = value.to_be_bytes();
                self.device.write_report_to_host(xfer, report_type);
            }

            // https://www.usb.org/sites/default/files/hid1_11.pdf p.62
            Request {
                request_type: RequestType::Class,
                recipient: Recipient::Interface,
                request: class_request::GET_IDLE,
                index: interface_number,
                value,
                ..
            } if self.is_my_interface(*interface_number) => {
                let [_zero, _report_id] = value.to_be_bytes();
                self.device.write_idle_mode_to_host(xfer);
            }

            // https://www.usb.org/sites/default/files/hid1_11.pdf p.64
            Request {
                request_type: RequestType::Class,
                recipient: Recipient::Interface,
                request: class_request::GET_PROTOCOL,
                index: interface_number,
                value: 0,
                ..
            } if self.is_my_interface(*interface_number) => {
                self.device.write_protocol_to_host(xfer);
            }

            // We don't care about any other case (it's not in the HID standard)
            _ => {
                //Leds::spawn_event(LedsEvent {
                //    right_red: true.into(),
                //    ..Default::default()
                //});
                //xfer.reject().ok();
                serial_msg!("other: {:?}", req.request_type);
            }
        }
    }

    /* Endpoint */

    fn endpoint_setup(&mut self, _addr: EndpointAddress) {}

    fn endpoint_out(&mut self, _addr: EndpointAddress) {}

    fn endpoint_in_complete(&mut self, _addr: EndpointAddress) {}
}

// Useful Constants:

pub const INTERFACE_CLASS_HID: u8 = 0x03;

#[allow(dead_code)]
#[derive(Clone, Copy, Debug, PartialEq)]
#[repr(u8)]
pub enum Subclass {
    None = 0x00,
    BootInterface = 0x01,
}

/// HID Interface Protocol.
#[derive(Clone, Copy, Debug, PartialEq)]
#[repr(u8)]
pub enum HidProtocol {
    None = 0x00,
    Keyboard = 0x01,
    Mouse = 0x02,
}

#[allow(dead_code)]
#[derive(Clone, Copy, Debug, PartialEq)]
#[repr(u8)]
pub enum CountryCode {
    NotSupported = 00,
    Arabic = 01,
    Belgian = 02,
    CanadianBilingual = 03,
    CanadiaFrench = 04,
    CzechRepublic = 05,
    Danish = 06,
    Finnish = 07,
    French = 08,
    German = 09,
    Greek = 10,
    Hebrew = 11,
    Hungary = 12,
    InternationalIso = 13,
    Italian = 14,
    JapanKatakana = 15,
    Korean = 16,
    LatinAmerican = 17,
    NetherlandsDutch = 18,
    Norwegian = 19,
    PersianFarsi = 20,
    Poland = 21,
    Portuguese = 22,
    Russia = 23,
    Slovakia = 24,
    Spanish = 25,
    Swedish = 26,
    SwissFrench = 27,
    SwissGerman = 28,
    Switzerland = 29,
    Taiwan = 30,
    TurkishQ = 31,
    Uk = 32,
    Us = 33,
    Yugoslavia = 34,
    TurkishF = 35,
}

#[derive(Debug, Clone, Copy)]
#[repr(u8)]
pub enum DescriptorType {
    Hid = 0x21,
    Report = 0x22,
    Physical = 0x23,
}

/// See section 7.2 Class-Specific Requests from the spec.
#[allow(dead_code)]
pub mod class_request {
    /// The Get_Report request allows the host to receive a report via the Control pipe.
    pub const GET_REPORT: u8 = 0x01;
    /// The Get_Idle request reads the current idle rate for a particular Input report (see:
    /// SET_IDLE request).
    pub const GET_IDLE: u8 = 0x02;
    /// The Get_Protocol request reads which protocol is currently active (either the boot
    /// protocol or the report protocol.)
    pub const GET_PROTOCOL: u8 = 0x03;
    /// The Set_Report request allows the host to send a report to the device, possibly
    /// setting the state of input, output, or feature controls.
    pub const SET_REPORT: u8 = 0x09;
    /// The Set_Idle request silences a particular report on the Interrupt In pipe until a
    /// new event occurs or the specified amount of time passes.
    pub const SET_IDLE: u8 = 0x0A;
    /// The Set_Protocol switches between the boot protocol and the report protocol (or
    /// vice versa).
    pub const SET_PROTOCOL: u8 = 0x0B;
}

impl PartialEq<DescriptorType> for u8 {
    fn eq(&self, other: &DescriptorType) -> bool {
        *self == *other as u8
    }
}
