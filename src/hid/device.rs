use crate::{
    leds::{Leds, LedsEvent},
    util::bit,
};
use hal::usb::UsbBusType;
use keebit::hid::Report;
use usb_device::class::{ControlIn, ControlOut};

/// The keyboard device.
#[derive(Default)]
pub struct Keyboard {
    protocol: KeyboardProtocol,
    report: Report,
}

#[derive(Clone, Copy, PartialEq)]
#[repr(u8)]
pub enum ReportType {
    Input = 1,
    Output = 2,
    Feature = 3,
}

#[derive(Clone, Copy, PartialEq, Default)]
#[repr(u8)]
pub enum KeyboardProtocol {
    Boot = 0,
    #[default]
    Report = 1,
}

impl Keyboard {
    /// Returns if there has been a change.
    pub fn set_report_and_has_change(&mut self, report: Report) -> bool {
        if report == self.report {
            false
        } else {
            self.report = report;
            true
        }
    }

    // HID setup

    pub fn max_packet_size(&self) -> u16 {
        8
    }

    pub fn report_descriptor(&self) -> &'static [u8] {
        REPORT_DESCRIPTOR
    }
}

// Response to the host.
impl Keyboard {
    pub fn write_report_descriptor_to_host<'a>(&mut self, xfer: ControlIn<UsbBusType>) {
        xfer.accept_with_static(self.report_descriptor()).ok();
    }

    pub fn write_report_to_host<'a>(&mut self, xfer: ControlIn<UsbBusType>, report_type: u8) {
        if report_type == ReportType::Input {
            xfer.accept_with(&self.report.into_bytes()).ok()
        } else {
            xfer.reject().ok()
        };
    }

    pub fn write_protocol_to_host<'a>(&mut self, xfer: ControlIn<UsbBusType>) {
        xfer.accept_with(&[self.protocol as u8]).ok();
    }

    pub fn write_idle_mode_to_host<'a>(&mut self, _xfer: ControlIn<UsbBusType>) {
        // Not handled at the moment:
    }
}

// Requests from the host.
impl Keyboard {
    // LEDs (the ones that indicate the status)

    pub fn read_report_from_host<'a>(&mut self, xfer: ControlOut<UsbBusType>, report_type: u8) {
        // The host asks to set the leds status:
        match xfer.data() {
            &[leds_status] if report_type == ReportType::Output => {
                self.set_num_lock(bit(leds_status, 0));
                self.set_caps_lock(bit(leds_status, 1));
                self.set_scroll_lock(bit(leds_status, 2));
                self.set_compose(bit(leds_status, 3));
                self.set_kana(bit(leds_status, 4));

                xfer.accept().ok()
            }
            _ => xfer.reject().ok(),
        };
    }

    fn set_num_lock(&mut self, value: bool) {
        Leds::spawn_event(LedsEvent {
            left_blue: value.into(),
            ..Default::default()
        });
    }

    fn set_caps_lock(&mut self, value: bool) {
        Leds::spawn_event(LedsEvent {
            left_green: value.into(),
            ..Default::default()
        });
    }

    /// <https://en.wikipedia.org/wiki/Scroll_Lock>
    fn set_scroll_lock(&mut self, _value: bool) {
        // Don't care
    }

    /// Show there is a composite input ongoing.
    fn set_compose(&mut self, _value: bool) {
        // Don't care
    }

    /// Japanese input.
    fn set_kana(&mut self, _value: bool) {
        // Don't care
    }

    // Request to be idle
    pub fn set_idle_for_duration(&mut self, _xfer: ControlOut<UsbBusType>, _duration: u8) {
        // Not handled at the moment:
    }

    // Request to be idle
    pub fn set_protocol(&mut self, xfer: ControlOut<UsbBusType>, protocol: u8) {
        match KeyboardProtocol::try_from(protocol) {
            //Ok(protocol @ KeyboardProtocol::Report) => {
            //    self.protocol = protocol;
            //    xfer.accept().ok();
            //}
            //Ok(KeyboardProtocol::Boot) => {
            //    xfer.reject().ok();
            //}
            Ok(protocol) => {
                self.protocol = protocol;
                xfer.accept().ok()
            }
            Err(()) => xfer.reject().ok(),
        };
    }
}

impl PartialEq<ReportType> for u8 {
    fn eq(&self, other: &ReportType) -> bool {
        *self == *other as u8
    }
}

impl TryFrom<u8> for KeyboardProtocol {
    type Error = ();

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        const BOOT: u8 = KeyboardProtocol::Boot as u8;
        const REPORT: u8 = KeyboardProtocol::Report as u8;

        match value {
            BOOT => Ok(Self::Boot),
            REPORT => Ok(Self::Report),
            _ => Err(()),
        }
    }
}

#[rustfmt::skip]
const REPORT_DESCRIPTOR: &[u8] = &[
    0x05, 0x01,         // Usage Page (Generic Desktop Ctrls)
    0x09, 0x06,         // Usage (Keyboard)
    0xA1, 0x01,         // Collection (Application)
    0x05, 0x07,         //   Usage Page (Kbrd/Keypad)
    /* Input: Modifiers byte */
    0x19, 0xE0,         //   Usage Minimum (0xE0) LeftControl
    0x29, 0xE7,         //   Usage Maximum (0xE7) RightGui
    0x15, 0x00,         //   Logical Minimum (0)
    0x25, 0x01,         //   Logical Maximum (1)
    0x95, 0x08,         //   Report Count (8)
    0x75, 0x01,         //   Report Size (1)
    0x81, 0x02,         //   Input (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position)
    /* Input: Reserved byte */
    0x95, 0x01,         //   Report Count (1)
    0x75, 0x08,         //   Report Size (8)
    0x81, 0x03,         //   Input (Const,Var,Abs,No Wrap,Linear,Preferred State,No Null Position)
    /* Input: Keys (6 maximum) */
    //0x05, 0x07,         //   Usage Page (Kbrd/Keypad)
    0x19, 0x00,         //   Usage Minimum (0x00)
    0x29, 0x81,         //   Usage Maximum (0x81)
    0x15, 0x00,         //   Logical Minimum (0)
    0x25, 0x81,         //   Logical Maximum (129)
    0x95, 0x06,         //   Report Count (6)
    0x75, 0x08,         //   Report Size (8)
    0x81, 0x00,         //   Input (Data,Array,Abs,No Wrap,Linear,Preferred State,No Null Position)
    /* Output: LEDs */
    0x05, 0x08,         //   Usage Page (LEDs)
    0x19, 0x01,         //   Usage Minimum (Num Lock)
    0x29, 0x05,         //   Usage Maximum (Kana)
    0x95, 0x05,         //   Report Count (5)
    0x75, 0x01,         //   Report Size (1)
    0x91, 0x02,         //   Output (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position,Non-volatile)
    /* Output: Reserved */
    0x95, 0x01,         //   Report Count (1)
    0x75, 0x03,         //   Report Size (3)
    0x91, 0x03,         //   Output (Const,Var,Abs,No Wrap,Linear,Preferred State,No Null Position,Non-volatile)
    0xC0,               // End Collection
];

#[rustfmt::skip]
const _REPORT_DESCRIPTOR_MAC: &[u8] = &[
    0x05, 0x01,         // Usage Page (Generic Desktop Ctrls)
    0x09, 0x06,         // Usage (Keyboard)
    0xA1, 0x01,         // Collection (Application)
    /* Input: Modifiers byte */
    0x05, 0x07,         //   Usage Page (Kbrd/Keypad)
    0x19, 0xE0,         //   Usage Minimum (0xE0)
    0x29, 0xE7,         //   Usage Maximum (0xE7)
    0x15, 0x00,         //   Logical Minimum (0)
    0x25, 0x01,         //   Logical Maximum (1)
    0x95, 0x08,         //   Report Count (8)
    0x75, 0x01,         //   Report Size (1)
    0x81, 0x02,         //   Input (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position)
    /* Input: Reserved byte */
    0x05, 0xFF,         //   Usage Page (AppleVendor Top Case)
    0x09, 0x03,         //   Usage (KeyboardFn)
    0x15, 0x00,         //   Logical Minimum (0)
    0x25, 0x01,         //   Logical Maximum (1)
    0x95, 0x01,         //   Report Count (1)
    0x75, 0x08,         //   Report Size (8)
    0x81, 0x02,         //   Input (Data, Variable, Absolute)
    /* Input: Keys (6 maximum) */
    0x05, 0x07,         //   Usage Page (Kbrd/Keypad)
    0x19, 0x00,         //   Usage Minimum (0x00)
    0x29, 0xFF,         //   Usage Maximum (0xFF) ???0x65
    0x15, 0x00,         //   Logical Minimum (0)
    0x26, 0xFF, 0x00,   //   Logical Maximum (255) ???0x65
    0x95, 0x06,         //   Report Count (6)
    0x75, 0x08,         //   Report Size (8)
    0x81, 0x00,         //   Input (Data,Array,Abs,No Wrap,Linear,Preferred State,No Null Position)
    /* Output: LEDs */
    0x05, 0x08,         //   Usage Page (LEDs)
    0x19, 0x01,         //   Usage Minimum (Num Lock)
    0x29, 0x05,         //   Usage Maximum (Kana)
    0x95, 0x05,         //   Report Count (5)
    0x75, 0x01,         //   Report Size (1)
    0x91, 0x02,         //   Output (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position,Non-volatile)
    /* Output: Reserved */
    0x95, 0x01,         //   Report Count (1)
    0x75, 0x03,         //   Report Size (3)
    0x91, 0x03,         //   Output (Const,Var,Abs,No Wrap,Linear,Preferred State,No Null Position,Non-volatile)
    0xC0,               // End Collection
];
