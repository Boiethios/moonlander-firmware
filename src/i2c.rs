//! Manages the slave/right part.
//!
//! The protocol is the following:
//! - 1st byte: the operation.
//! - 2nd byte: the first led & the matrix column to active
//! - 3rd byte: the second & third led

use core::ops::Not;

use hal::{
    gpio::{gpiob, Alternate, Gpiob, Input, OpenDrain, Pin, U},
    i2c::I2c,
    pac::I2C1,
    prelude::*,
    rcc::{Clocks, APB1},
};

use crate::leds::Leds;

const ADDR: u8 = 0x20;

type MyI2c = I2c<
    I2C1,
    (
        hal::gpio::Pin<hal::gpio::Gpiob, hal::gpio::U<6>, Alternate<OpenDrain, 4>>,
        hal::gpio::Pin<hal::gpio::Gpiob, hal::gpio::U<7>, Alternate<OpenDrain, 4>>,
    ),
>;

//TODO have 2 struct: one init, the other one uninit (either because the part is disconnected or being initialized)
pub struct RightI2c {
    i2c: MyI2c,
}

impl RightI2c {
    pub fn new(
        i2c1: I2C1,
        (pb6, pb7): (Pin<Gpiob, U<6>, Input>, Pin<Gpiob, U<7>, Input>),
        (moder, otyper, afrl, pupdr): (
            &mut gpiob::MODER,
            &mut gpiob::OTYPER,
            &mut gpiob::AFRL,
            &mut gpiob::PUPDR,
        ),
        clocks: Clocks,
        apb1: &mut APB1,
    ) -> Self {
        let mut scl = pb6.into_af_open_drain(moder, otyper, afrl);
        let mut sda = pb7.into_af_open_drain(moder, otyper, afrl);
        scl.internal_pull_up(pupdr, true);
        sda.internal_pull_up(pupdr, true);

        let i2c = hal::i2c::I2c::new(
            i2c1,
            (scl, sda),
            100.kHz().try_into().unwrap(),
            clocks,
            apb1,
        );

        RightI2c { i2c }.init()
    }

    fn init(mut self) -> Self {
        if let Err(_) = self
            .i2c
            // Turn the LEDs on:
            // IODIRA -  A is output - B is inputs:
            .write(ADDR, &[0x00, 0b00000000, 0b00111111])
            // GPPUA - A is not pulled-up - B is pulled-up:
            .and_then(|_| self.i2c.write(ADDR, &[0x0C, 0b10000000, 0b11111111]))
        {
            // Do nothing for now
        }

        self
    }

    pub fn scanner(&mut self) -> I2cScanner<'_> {
        let (blue, green, red) = Leds::right();
        let red = u8::from(red) << 7;
        let green = u8::from(green) << 6;
        let blue = u8::from(blue) << 7;

        I2cScanner {
            i2c: self,
            blue,
            green,
            red,
        }
    }
}

pub struct I2cScanner<'a> {
    i2c: &'a mut RightI2c,
    blue: u8,
    green: u8,
    red: u8,
}

impl I2cScanner<'_> {
    pub fn write(&mut self, col: usize) {
        let col = 1 << col;
        let payload = [0x12, (self.red | col).not(), (self.blue | self.green).not()];

        if self.i2c.i2c.write(ADDR, &payload).is_err() {
            crate::leds::Leds::spawn_event(crate::leds::LedsEvent {
                left_red: crate::leds::On,
                ..Default::default()
            });
        }
    }

    pub fn read(&mut self) -> [bool; crate::config::ROWS] {
        let mut buffer = 0;

        if self
            .i2c
            .i2c
            .write_read(ADDR, &[0x13], core::slice::from_mut(&mut buffer))
            .is_err()
        {
            crate::leds::Leds::spawn_event(crate::leds::LedsEvent {
                left_red: crate::leds::On,
                ..Default::default()
            });
        }
        let buffer = (buffer & 0b00_111111).not();

        [
            buffer & 0b00_100000 != 0,
            buffer & 0b00_010000 != 0,
            buffer & 0b00_001000 != 0,
            buffer & 0b00_000100 != 0,
            buffer & 0b00_000010 != 0,
            buffer & 0b00_000001 != 0,
        ]
    }
}
