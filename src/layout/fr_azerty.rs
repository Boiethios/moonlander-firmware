//! French keyboard layout.

use keebit::{
    layout::actions::{ConstFrom, KeyPress},
    prelude::*,
};

const ALT_GR: u8 = RIGHT_ALT;
const SHIFT: u8 = LEFT_SHIFT;

//const fn kp(k: impl ~const ReportKeys) -> KeyPress {
//    let report = k.const_into();
//
//    panic!()
//}

/// Keys for Linux OSes.
pub mod linux {
    use super::*;

    /*

    /// Dead keys:
    ///
    /// - `'` Accent aigu
    /// - `` ` `` Accent grave
    /// - `^` Accent circonflexe / exposant
    /// - `"` Trémas
    /// - `ˇ` Voyelle courte
    /// - `¯` Voyelle longue
    /// - `¸` Cédille
    /// - `°` Petit rond (??)
    /// - `~` Tilde (??)
    pub const fn modif<T>(c: char) -> T
    where
        T: From<KeyPress>,
    {
        From::from(match c {
            '\'' => k(AltGr + Apostrophe),
            '`' => k(AltGr + HashTilde),
            '^' => k(LeftBrace),
            '¨' => k(Shift + LeftBrace),
            'ˇ' => k(AltGr + Kb1),
            '¯' => k(AltGr + Shift + HashTilde),
            '¸' => k(AltGr + Shift + Grave),
            '°' => k(AltGr + Shift + LeftBrace),
            '~' => k(AltGr + LeftBrace),
            _ => panic!("Not a (supported) modifier"),
        })
    }
    */

    pub const fn dead_key<T>(c: char) -> T
    where
        T: ~const ConstFrom<KeyPress>,
    {
        ConstFrom::const_from(match c {
            '^' => k(LeftBrace),
            '¨' => km(LeftBrace, SHIFT),
            '~' => km(LeftBrace, ALT_GR),
            '`' => km(HashTilde, ALT_GR),
            '´' => km(Apostrophe, ALT_GR),
            _ => panic!("Not a (supported) dead key"),
        })
    }

    /// Key presses for Linux.
    ///
    /// The numpad version is not used in the returned keycodes.
    pub const fn key<T>(c: char) -> T
    where
        T: ~const ConstFrom<KeyPress>,
    {
        ConstFrom::const_from(match c {
            // ASCII:
            '!' => k::<KeyPress>(Slash),
            '"' => k::<KeyPress>(Kb3),
            '#' => km::<KeyPress>(Kb3, ALT_GR),
            '$' => k::<KeyPress>(RightBrace),
            '%' => km::<KeyPress>(Apostrophe, SHIFT),
            '&' => k::<KeyPress>(Kb1),
            '\'' => k::<KeyPress>(Kb4),
            '(' => k::<KeyPress>(Kb5),
            ')' => k::<KeyPress>(Minus),
            '*' => k::<KeyPress>(HashTilde),
            '+' => km::<KeyPress>(Equal, SHIFT),
            ',' => k::<KeyPress>(M),
            '-' => k::<KeyPress>(Kb6),
            '.' => km::<KeyPress>(Comma, SHIFT),
            '/' => km::<KeyPress>(Dot, SHIFT),
            '0' => km::<KeyPress>(Kb0, SHIFT),
            '1' => km::<KeyPress>(Kb1, SHIFT),
            '2' => km::<KeyPress>(Kb2, SHIFT),
            '3' => km::<KeyPress>(Kb3, SHIFT),
            '4' => km::<KeyPress>(Kb4, SHIFT),
            '5' => km::<KeyPress>(Kb5, SHIFT),
            '6' => km::<KeyPress>(Kb6, SHIFT),
            '7' => km::<KeyPress>(Kb7, SHIFT),
            '8' => km::<KeyPress>(Kb8, SHIFT),
            '9' => km::<KeyPress>(Kb9, SHIFT),
            ':' => k::<KeyPress>(Dot),
            ';' => k::<KeyPress>(Comma),
            '<' => k::<KeyPress>(_102ND),
            '=' => k::<KeyPress>(Equal),
            '>' => km::<KeyPress>(_102ND, SHIFT),
            '?' => km::<KeyPress>(M, SHIFT),
            '@' => km::<KeyPress>(Kb0, ALT_GR),

            'A' => km::<KeyPress>(Q, SHIFT),
            'B' => km::<KeyPress>(B, SHIFT),
            'C' => km::<KeyPress>(C, SHIFT),
            'D' => km::<KeyPress>(D, SHIFT),
            'E' => km::<KeyPress>(E, SHIFT),
            'F' => km::<KeyPress>(F, SHIFT),
            'G' => km::<KeyPress>(G, SHIFT),
            'H' => km::<KeyPress>(H, SHIFT),
            'I' => km::<KeyPress>(I, SHIFT),
            'J' => km::<KeyPress>(J, SHIFT),
            'K' => km::<KeyPress>(K, SHIFT),
            'L' => km::<KeyPress>(L, SHIFT),
            'M' => km::<KeyPress>(Semicolon, SHIFT),
            'N' => km::<KeyPress>(N, SHIFT),
            'O' => km::<KeyPress>(O, SHIFT),
            'P' => km::<KeyPress>(P, SHIFT),
            'Q' => km::<KeyPress>(A, SHIFT),
            'R' => km::<KeyPress>(R, SHIFT),
            'S' => km::<KeyPress>(S, SHIFT),
            'T' => km::<KeyPress>(T, SHIFT),
            'U' => km::<KeyPress>(U, SHIFT),
            'V' => km::<KeyPress>(V, SHIFT),
            'W' => km::<KeyPress>(Z, SHIFT),
            'X' => km::<KeyPress>(X, SHIFT),
            'Y' => km::<KeyPress>(Y, SHIFT),
            'Z' => km::<KeyPress>(W, SHIFT),

            '[' => km::<KeyPress>(Kb5, ALT_GR),
            '\\' => km::<KeyPress>(Kb8, ALT_GR),
            ']' => km::<KeyPress>(Minus, ALT_GR),
            '^' => km::<KeyPress>(Kb9, ALT_GR),
            '_' => k::<KeyPress>(Kb8),
            '`' => km::<KeyPress>(Kb7, ALT_GR),

            'a' => k::<KeyPress>(Q),
            'b' => k::<KeyPress>(B),
            'c' => k::<KeyPress>(C),
            'd' => k::<KeyPress>(D),
            'e' => k::<KeyPress>(E),
            'f' => k::<KeyPress>(F),
            'g' => k::<KeyPress>(G),
            'h' => k::<KeyPress>(H),
            'i' => k::<KeyPress>(I),
            'j' => k::<KeyPress>(J),
            'k' => k::<KeyPress>(K),
            'l' => k::<KeyPress>(L),
            'm' => k::<KeyPress>(Semicolon),
            'n' => k::<KeyPress>(N),
            'o' => k::<KeyPress>(O),
            'p' => k::<KeyPress>(P),
            'q' => k::<KeyPress>(A),
            'r' => k::<KeyPress>(R),
            's' => k::<KeyPress>(S),
            't' => k::<KeyPress>(T),
            'u' => k::<KeyPress>(U),
            'v' => k::<KeyPress>(V),
            'w' => k::<KeyPress>(Z),
            'x' => k::<KeyPress>(X),
            'y' => k::<KeyPress>(Y),
            'z' => k::<KeyPress>(W),

            '{' => km::<KeyPress>(Kb4, ALT_GR),
            '|' => km::<KeyPress>(Kb6, ALT_GR),
            '}' => km::<KeyPress>(Equal, ALT_GR),
            '~' => km::<KeyPress>(Kb2, ALT_GR),

            // French specifics:
            'é' => k::<KeyPress>(Kb2),
            'è' => k::<KeyPress>(Kb7),
            'à' => k::<KeyPress>(Kb0),
            'ù' => k::<KeyPress>(Apostrophe),
            'ç' => k::<KeyPress>(Kb9),
            'æ' => km::<KeyPress>(Q, ALT_GR),
            'œ' => km::<KeyPress>(O, ALT_GR),
            'â' => km::<KeyPress>(W, ALT_GR),
            'ê' => km::<KeyPress>(R, ALT_GR),
            'î' => km::<KeyPress>(I, ALT_GR),
            'ô' => km::<KeyPress>(P, ALT_GR),
            'û' => km::<KeyPress>(U, ALT_GR),
            'ä' => km::<KeyPress>(A, ALT_GR),
            'ë' => km::<KeyPress>(D, ALT_GR),
            'ï' => km::<KeyPress>(K, ALT_GR),
            'ö' => km::<KeyPress>(Apostrophe, ALT_GR),
            'ü' => km::<KeyPress>(J, ALT_GR),
            'ÿ' => km::<KeyPress>(Y, ALT_GR),
            'ŀ' => km::<KeyPress>(L, ALT_GR),

            'É' => km::<KeyPress>(Kb2, SHIFT + ALT_GR),
            'È' => km::<KeyPress>(Kb7, SHIFT + ALT_GR),
            'À' => km::<KeyPress>(Kb0, SHIFT + ALT_GR),
            'Ù' => km::<KeyPress>(Apostrophe, SHIFT + ALT_GR),
            'Ç' => km::<KeyPress>(Kb9, SHIFT + ALT_GR),
            'Æ' => km::<KeyPress>(Q, SHIFT + ALT_GR),
            'Œ' => km::<KeyPress>(O, SHIFT + ALT_GR),
            'Â' => km::<KeyPress>(W, SHIFT + ALT_GR),
            'Ê' => km::<KeyPress>(R, SHIFT + ALT_GR),
            'Î' => km::<KeyPress>(I, SHIFT + ALT_GR),
            'Ô' => km::<KeyPress>(P, SHIFT + ALT_GR),
            'Û' => km::<KeyPress>(U, SHIFT + ALT_GR),
            'Ä' => km::<KeyPress>(A, SHIFT + ALT_GR),
            'Ë' => km::<KeyPress>(D, SHIFT + ALT_GR),
            'Ï' => km::<KeyPress>(K, SHIFT + ALT_GR),
            'Ö' => km::<KeyPress>(Apostrophe, SHIFT + ALT_GR),
            'Ü' => km::<KeyPress>(J, SHIFT + ALT_GR),
            'Ÿ' => km::<KeyPress>(Y, SHIFT + ALT_GR),
            'Ŀ' => km::<KeyPress>(L, SHIFT + ALT_GR),

            '…' => km::<KeyPress>(M, SHIFT + ALT_GR),
            '«' => km::<KeyPress>(Z, ALT_GR),
            '»' => km::<KeyPress>(X, ALT_GR),
            '„' => km::<KeyPress>(S, SHIFT + ALT_GR),
            '“' => km::<KeyPress>(Z, SHIFT + ALT_GR),
            '”' => km::<KeyPress>(X, SHIFT + ALT_GR),

            // Other:
            '€' => km::<KeyPress>(E, ALT_GR),
            '°' => km::<KeyPress>(Minus, SHIFT),

            '←' => km::<KeyPress>(V, SHIFT + ALT_GR),
            '→' => km::<KeyPress>(N, SHIFT + ALT_GR),
            '↓' => km::<KeyPress>(B, ALT_GR),
            '↑' => km::<KeyPress>(B, SHIFT + ALT_GR),

            _ => unimplemented!(),
        })
    }

    pub const fn copy<T>() -> T
    where
        T: ~const ConstFrom<KeyPress>,
    {
        ConstFrom::const_from(km(Insert, LEFT_CTRL))
    }

    pub const fn cut<T>() -> T
    where
        T: ~const ConstFrom<KeyPress>,
    {
        ConstFrom::const_from(km(Delete, LEFT_SHIFT))
    }

    pub const fn paste<T>() -> T
    where
        T: ~const ConstFrom<KeyPress>,
    {
        ConstFrom::const_from(km(Insert, LEFT_SHIFT))
    }
}
