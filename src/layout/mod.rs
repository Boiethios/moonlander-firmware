mod fr_azerty;

use crate::{
    bootload,
    config::{FULL_COLS, ROWS},
};
use fr_azerty::linux::{copy, cut, dead_key, key, paste};
use keebit::{
    hid::{KeyCode, LeftShift, RightAlt},
    prelude::*,
};

const BASE: u8 = 0;
const DIACRITICS: u8 = 1;
const NUMPAD: u8 = 2;
const SHORTCUTS: u8 = 3;
const MULTIMEDIA: u8 = 4;
const GAMING: u8 = 5;

const TRIPLE_BACKQUOTES: [Report; 3] = [
    Report::builder().k(Kb7).m(RightAlt).build(),
    Report::builder().k(Kb7).m(RightAlt).build(),
    Report::builder().k(Kb7).m(RightAlt).build(),
];

const GRAVE: Report = Report::builder().k(HashTilde).m(RightAlt).build();
const ACUTE: Report = Report::builder().k(Apostrophe).m(RightAlt).build();

const fn lower(letter: KeyCode) -> Report {
    Report::builder().k(letter).build()
}
const fn upper(letter: KeyCode) -> Report {
    Report::builder().m(LeftShift).k(letter).build()
}

pub type MyLayout = Layout<crate::MyMonotonicTimer, (), 5, 2, 6, ROWS, FULL_COLS>;
#[inline(always)]
pub fn create() -> Result<MyLayout, Error> {
    Layout::builder(
        (),
        &[
            L0_BASE,
            L1_DIACRITICS,
            L2_NUMPAD,
            L3_SHORTCUTS,
            L4_MULTIMEDIA,
            L5_GAMING,
        ],
    )
    .with_below(|layer| match layer {
        BASE => None,
        DIACRITICS => Some(BASE),
        NUMPAD => Some(BASE),
        SHORTCUTS => Some(BASE),
        MULTIMEDIA => Some(BASE),
        GAMING => Some(BASE),
        _not_a_layer => None,
    })
    .with_timer(crate::MyMonotonicTimer)
    .with_debounce_time(25)
    .with_default_timeout(175)
    .with_chords([
        (&[(5, 0), (5, 1)], k(Enter)),
        //(&[(5, 1), (5, 2)], m(LEFT_CTRL)),
        (&[(5, 3), (5, 10)], custom(action_reset)),
        (&[(0, 0), (0, 6)], k(NumLock)),
        (&[(0, 7), (0, 13)], k(CapsLock)),
        (&[(0, 0), (4, 0)], sw_l(GAMING)),
    ])
    .build()
}

fn action_reset(
    _reports: &mut Reports,
    _state: &mut LayoutState,
    _monotonic: &mut dyn MonotonicTimer,
    _event: Event,
) {
    bootload::reset()
}

const L0_BASE: [[MaybeAction; FULL_COLS]; ROWS] = [
    [
        k(Esc),
        XXXXX,
        XXXXX,
        XXXXX,
        XXXXX,
        XXXXX,
        XXXXX, /* Right */
        XXXXX,
        XXXXX,
        XXXXX,
        XXXXX,
        XXXXX,
        XXXXX,
        XXXXX,
    ],
    [
        k(Tab),
        key('b'),
        key('m'),
        key('p'),
        key('o'),
        key('_'),
        k(Cut), /* Right */
        XXXXX,
        hold(dead_key('^'), key('!')),
        key('v'),
        key('d'),
        key('l'),
        key('j'),
        key('z'),
    ],
    [
        m(LEFT_SHIFT),
        key('a'),
        key('u'),
        key('i'),
        key('e'),
        hold(key(','), key(';')),
        hold(k(Copy), key('x')), /* Right */
        XXXXX,
        key('c'),
        key('t'),
        key('s'),
        key('r'),
        key('n'),
        m(LEFT_SHIFT),
    ],
    [
        XXXXX,
        key('w'),
        key('y'),
        key('x'),
        hold(key('.'), key(':')),
        key('k'),
        XXXXX, /* Right */
        XXXXX,
        hold(key('\''), key('?')),
        key('q'),
        key('g'),
        key('h'),
        key('f'),
        XXXXX,
    ],
    [
        XXXXX,
        XXXXX,
        XXXXX,
        l(MULTIMEDIA),
        l(NUMPAD),
        XXXXX,
        XXXXX, /* Right */
        XXXXX,
        XXXXX,
        k(Left),
        k(Down),
        k(Up),
        k(Right),
        XXXXX,
    ],
    [
        k(Space),
        m(LEFT_GUI),
        l(SHORTCUTS),
        m(LEFT_CTRL),
        XXXXX,
        XXXXX,
        XXXXX, /* Right */
        XXXXX,
        XXXXX,
        XXXXX,
        m(RIGHT_CTRL),
        l(SHORTCUTS),
        l(DIACRITICS),
        k(Backspace),
    ],
];

const L1_DIACRITICS: [[MaybeAction; FULL_COLS]; ROWS] = [
    [
        XXXXX, XXXXX, XXXXX, XXXXX, XXXXX, XXXXX, XXXXX, /* Right */
        XXXXX, XXXXX, XXXXX, XXXXX, XXXXX, XXXXX, XXXXX,
    ],
    [
        hold(key('æ'), key('Æ')),
        key('('),
        key(')'),
        hold(key('œ'), key('Œ')),
        hold(macr(&[ACUTE, lower(O)]), macr(&[ACUTE, upper(O)])),
        hold(macr(&[GRAVE, lower(O)]), macr(&[GRAVE, upper(O)])),
        XXXXX, /* Right */
        XXXXX,
        key('^'),
        key('«'),
        key('»'),
        hold(key('ŀ'), key('Ŀ')),
        dead_key('¨'),
        key('°'),
    ],
    [
        _____,
        hold(key('à'), key('À')),
        hold(key('ù'), key('Ù')),
        hold(macr(&[GRAVE, lower(I)]), macr(&[GRAVE, upper(I)])),
        hold(key('é'), key('É')),
        hold(key('è'), key('È')),
        XXXXX, /* Right */
        XXXXX,
        hold(key('ç'), key('Ç')),
        key('←'),
        key('↓'),
        key('↑'),
        key('→'),
        _____,
    ],
    [
        XXXXX,
        hold(macr(&[ACUTE, lower(Q)]), macr(&[ACUTE, upper(Q)])),
        hold(macr(&[ACUTE, lower(U)]), macr(&[ACUTE, upper(U)])),
        hold(macr(&[ACUTE, lower(I)]), macr(&[ACUTE, upper(I)])),
        key('…'),
        key('€'),
        XXXXX, /* Right */
        XXXXX,
        dead_key('~'),
        key('@'),
        key('„'),
        key('“'),
        key('”'),
        XXXXX,
    ],
    [
        XXXXX, XXXXX, XXXXX, _____, _____, XXXXX, XXXXX, /* Right */
        XXXXX, XXXXX, _____, _____, XXXXX, XXXXX, XXXXX,
    ],
    [
        _____, _____, _____, _____, XXXXX, XXXXX, XXXXX, /* Right */
        XXXXX, XXXXX, XXXXX, _____, _____, _____, _____,
    ],
];

const L2_NUMPAD: [[MaybeAction; FULL_COLS]; ROWS] = [
    [
        XXXXX, XXXXX, XXXXX, XXXXX, XXXXX, XXXXX, XXXXX, /* Right */
        XXXXX, XXXXX, XXXXX, XXXXX, XXXXX, XXXXX, XXXXX,
    ],
    [
        k(KpSlash),
        k(Kp1),
        k(Kp2),
        k(Kp3),
        k(KpEqual),
        _____,
        XXXXX, /* Right */
        XXXXX,
        key('`'),
        key('['),
        key(']'),
        key('|'),
        key('#'),
        key('%'),
    ],
    [
        k(KpAsterisk),
        k(Kp4),
        k(Kp5),
        k(Kp6),
        k(Kp0),
        k(KpMinus),
        XXXXX, /* Right */
        XXXXX,
        key('$'),
        key('{'),
        key('}'),
        key('&'),
        key('"'),
        _____,
    ],
    [
        XXXXX,
        k(Kp7),
        k(Kp8),
        k(Kp9),
        k(KpDot),
        k(KpPlus),
        XXXXX, /* Right */
        XXXXX,
        key('~'),
        key('<'),
        key('>'),
        key('\\'),
        MaybeAction::Nothing,
        XXXXX,
    ],
    [
        XXXXX, XXXXX, XXXXX, _____, _____, XXXXX, XXXXX, /* Right */
        XXXXX, XXXXX, _____, _____, _____, _____, XXXXX,
    ],
    [
        _____, _____, _____, _____, XXXXX, XXXXX, XXXXX, /* Right */
        XXXXX, XXXXX, XXXXX, _____, _____, _____, _____,
    ],
];

const L3_SHORTCUTS: [[MaybeAction; FULL_COLS]; ROWS] = [
    [
        k(Esc),
        k(F1),
        k(F2),
        k(F3),
        k(F4),
        k(F5),
        k(F6), /* Right */
        XXXXX,
        XXXXX,
        XXXXX,
        XXXXX,
        XXXXX,
        XXXXX,
        XXXXX,
    ],
    [
        k(Insert),
        k(F1),
        k(F2),
        k(F3),
        k(Delete),
        MaybeAction::Nothing,
        k(F7), /* Right */
        XXXXX,
        macr(&TRIPLE_BACKQUOTES),
        k(Home),
        k(PageDown),
        k(PageUp),
        k(End),
        k(X), //TODO =>
    ],
    [
        _____,
        k(F4),
        k(F5),
        k(F6),
        k(F10),
        MaybeAction::Nothing,
        k(Paste), /* Right */
        k(F8),
        k(X), //TODO ->
        k(Left),
        k(Down),
        k(Up),
        k(Right),
        _____,
    ],
    [
        XXXXX,
        k(F7),
        k(F8),
        k(F9),
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        XXXXX, /* Right */
        XXXXX,
        MaybeAction::Nothing,
        km(Left, LEFT_GUI + LEFT_ALT),
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        km(Right, LEFT_GUI + LEFT_ALT),
        XXXXX,
    ],
    [
        XXXXX,
        XXXXX,
        XXXXX,
        _____,
        _____,
        XXXXX,
        XXXXX, /* Right */
        XXXXX,
        XXXXX,
        k(Home),
        k(Down),
        k(Up),
        k(End),
        XXXXX,
    ],
    [
        _____, _____, _____, _____, XXXXX, XXXXX, XXXXX, /* Right */
        XXXXX, XXXXX, XXXXX, _____, _____, _____, _____,
    ],
];

const L4_MULTIMEDIA: [[MaybeAction; FULL_COLS]; ROWS] = [
    [
        XXXXX, XXXXX, XXXXX, XXXXX, XXXXX, XXXXX, XXXXX, /* Right */
        XXXXX, XXXXX, XXXXX, XXXXX, XXXXX, XXXXX, XXXXX,
    ],
    [
        MaybeAction::Nothing,
        km(S, LEFT_CTRL),
        k(Undo),
        k(Again),
        k(Front),
        MaybeAction::Nothing,
        XXXXX, /* Right */
        XXXXX,
        MaybeAction::Nothing,
        k(Left),
        k(VolumeDown),
        k(VolumeUp),
        MaybeAction::Nothing,
        MaybeAction::Nothing,
    ],
    [
        k(F13),
        k(F14),
        k(F15),
        k(F16),
        k(F17),
        k(F18),
        XXXXX, /* Right */
        XXXXX,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        k(Mute),
        MaybeAction::Nothing,
        MaybeAction::Nothing,
    ],
    [
        XXXXX,
        k(F19),
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        XXXXX, /* Right */
        XXXXX,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        XXXXX,
    ],
    [
        XXXXX, XXXXX, XXXXX, _____, _____, XXXXX, XXXXX, /* Right */
        XXXXX, XXXXX, _____, _____, _____, _____, XXXXX,
    ],
    [
        _____, _____, _____, _____, XXXXX, XXXXX, XXXXX, /* Right */
        XXXXX, XXXXX, XXXXX, _____, _____, _____, _____,
    ],
];

const L5_GAMING: [[MaybeAction; FULL_COLS]; ROWS] = [
    [
        k(Esc),
        k(Kb1),
        k(Kb2),
        k(Kb3),
        k(Kb4),
        k(Kb5),
        k(Kb6), /* Right */
        XXXXX,
        XXXXX,
        XXXXX,
        XXXXX,
        XXXXX,
        XXXXX,
        XXXXX,
    ],
    [
        k(Tab),
        k(A),
        k(Z),
        k(E),
        k(R),
        k(T),
        k(Kb7), /* Right */
        XXXXX,
        MaybeAction::Nothing,
        k(Left),
        k(VolumeDown),
        k(VolumeUp),
        MaybeAction::Nothing,
        MaybeAction::Nothing,
    ],
    [
        m(LEFT_SHIFT),
        k(Q),
        k(S),
        k(D),
        k(F),
        k(G),
        k(Kb8), /* Right */
        XXXXX,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        k(Mute),
        MaybeAction::Nothing,
        MaybeAction::Nothing,
    ],
    [
        XXXXX,
        k(W),
        k(X),
        k(C),
        k(V),
        k(B),
        XXXXX, /* Right */
        XXXXX,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        XXXXX,
    ],
    [
        XXXXX, XXXXX, XXXXX, _____, _____, XXXXX, XXXXX, /* Right */
        XXXXX, XXXXX, _____, _____, _____, _____, XXXXX,
    ],
    [
        _____, _____, _____, _____, XXXXX, XXXXX, XXXXX, /* Right */
        XXXXX, XXXXX, XXXXX, _____, _____, _____, _____,
    ],
];

const _TEMPLATE: [[MaybeAction; FULL_COLS]; ROWS] = [
    [
        XXXXX, XXXXX, XXXXX, XXXXX, XXXXX, XXXXX, XXXXX, /* Right */
        XXXXX, XXXXX, XXXXX, XXXXX, XXXXX, XXXXX, XXXXX,
    ],
    [
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        XXXXX, /* Right */
        XXXXX,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
    ],
    [
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        XXXXX, /* Right */
        XXXXX,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
    ],
    [
        XXXXX,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        XXXXX, /* Right */
        XXXXX,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        XXXXX,
    ],
    [
        XXXXX,
        XXXXX,
        XXXXX,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        XXXXX,
        XXXXX, /* Right */
        XXXXX,
        XXXXX,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        XXXXX,
        XXXXX,
        XXXXX,
    ],
    [
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        XXXXX,
        XXXXX,
        XXXXX, /* Right */
        XXXXX,
        XXXXX,
        XXXXX,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
        MaybeAction::Nothing,
    ],
];

/*
//#[rustfmt::skip]
#[inline(always)]
fn main_layer() -> [[MaybeAction; FULL_COLS]; ROWS] {
    [
        [
            k(Esc),
            hold(key('"'), key('1')),
            hold(key('«'), key('2')),
            hold(key('»'), key('3')),
            hold(key('('), key('4')),
            hold(key(')'), key('5')),
            hold(key('$'), key('#')),
            /* Right */
            hold(key('%'), key('`')),
            hold(key('@'), key('6')),
            hold(key('+'), key('7')),
            hold(key('-'), key('8')),
            hold(key('/'), key('9')),
            hold(key('*'), key('0')),
            hold(key('='), key('°')),
        ],
        [
            k(Tab),
            hold(key('b'), key('B')),
            hold(key('é'), key('É')),
            hold(key('p'), key('P')),
            hold(key('o'), key('O')),
            hold(key('è'), key('È')),
            k(Undo),
/* Right */
            k(Insert),
            hold(key('^'), key('!')),
            hold(key('v'), key('V')),
            hold(key('d'), key('D')),
            hold(key('l'), key('L')),
            hold(key('j'), key('J')),
            hold(key('z'), key('Z')),
        ],
        [
            m(LeftShift),
            hold(key('a'), key('A')),
            hold(key('u'), key('U')),
            hold(key('i'), key('I')),
            hold(key('e'), key('E')),
            hold(key(','), key(';')),
            hold(k(LeftCtrl + Insert), k(LeftShift + Insert)),
/* Right */
            MaybeAction::Nothing,
            hold(key('c'), key('C')),
            hold(key('t'), key('T')),
            hold(key('s'), key('S')),
            hold(key('r'), key('R')),
            hold(key('n'), key('N')),
            m(RightShift),
        ],
        [
            key('à'),
            hold(key('w'), key('W')),
            hold(key('y'), key('Y')),
            hold(key('x'), key('X')),
            hold(key('.'), key(':')),
            hold(key('k'), key('K')),
            XXXXX,
/* Right */
            XXXXX,
            hold(key('\''), key('?')),
            hold(key('q'), key('Q')),
            hold(key('g'), key('G')),
            hold(key('h'), key('H')),
            hold(key('f'), key('F')),
            hold(key('m'), key('M')),
        ],
        [
            m(LeftAlt),
            MaybeAction::Nothing,
            MaybeAction::Nothing,
            MaybeAction::Nothing,
            k(Delete),
            XXXXX,
            XXXXX,
/* Right */
            XXXXX,
            XXXXX,
            k(Left),
            k(Down),
            k(Up),
            k(Right),
            MaybeAction::Nothing,
        ],
        [
            k(Space),
            m(LeftGui),
            l(SYMBOLS),
//l(SHORTCUTS),
            m(LeftCtrl),
            XXXXX,
            XXXXX,
            XXXXX,
/* Right */
            XXXXX,
            XXXXX,
            XXXXX,
            m(RightCtrl),
//l(SHORTCUTS),
            l(SYMBOLS),
            l(SYMBOLS),
            k(Backspace),
        ],
    ]
}

/// A layer used with modifier. It allows to have both the shortcut with and without shift,
/// by using a long press for the shift variant.
#[inline(always)]
fn shift_hold_layer() -> [[MaybeAction; FULL_COLS]; ROWS] {
    [
        [
            k(Esc),
            hold(k(Kb1), k(LeftShift + Kb1)),
            hold(k(Kb2), k(LeftShift + Kb2)),
            hold(k(Kb3), k(LeftShift + Kb3)),
            hold(k(Kb4), k(LeftShift + Kb4)),
            hold(k(Kb5), k(LeftShift + Kb5)),
            key('$'),
/* Right */
            key('%'),
            hold(k(Kb6), k(LeftShift + Kb6)),
            hold(k(Kb7), k(LeftShift + Kb7)),
            hold(k(Kb8), k(LeftShift + Kb8)),
            hold(k(Kb9), k(LeftShift + Kb9)),
            hold(k(Kb0), k(LeftShift + Kb0)),
            key('='),
        ],
        [
            k(Tab),
            hold(key('b'), key('B')),
            MaybeAction::Nothing,
            hold(key('p'), key('P')),
            hold(key('o'), key('O')),
            MaybeAction::Nothing,
            MaybeAction::Nothing,
/* Right */
            MaybeAction::Nothing,
            hold(key('^'), key('!')),
            hold(key('v'), key('V')),
            hold(key('d'), key('D')),
            hold(key('l'), key('L')),
            hold(key('j'), key('J')),
            hold(key('z'), key('Z')),
        ],
        [
            k(Backspace),
            hold(key('a'), key('A')),
            hold(key('u'), key('U')),
            hold(key('i'), key('I')),
            hold(key('e'), key('E')),
            hold(key(','), key(';')),
            _____,
/* Right */
            _____,
            hold(key('c'), key('C')),
            hold(key('t'), key('T')),
            hold(key('s'), key('S')),
            hold(key('r'), key('R')),
            hold(key('n'), key('N')),
            hold(key('m'), key('M')),
        ],
        [
            m(LeftShift),
            hold(key('w'), key('W')),
            hold(key('y'), key('Y')),
            hold(key('x'), key('X')),
            hold(key('.'), key(':')),
            hold(key('k'), key('K')),
            XXXXX,
/* Right */
            XXXXX,
            hold(key('\''), key('?')),
            hold(key('q'), key('Q')),
            hold(key('g'), key('G')),
            hold(key('h'), key('H')),
            hold(key('f'), key('F')),
            hold(key('&'), key('_')),
        ],
        [
            m(LeftAlt),
            MaybeAction::Nothing,
            hold(key('à'), key('À')),
            hold(key('{'), key('}')),
            hold(key('['), key(']')),
            XXXXX,
            XXXXX,
/* Right */
            XXXXX,
            XXXXX,
            k(Left),
            k(Down),
            k(Up),
            k(Right),
            MaybeAction::Nothing,
        ],
        [
            k(Space),
            m(LeftGui),
            l(1),
            m(LeftCtrl),
            XXXXX,
            XXXXX,
            XXXXX,
/* Right */
            XXXXX,
            XXXXX,
            XXXXX,
            MaybeAction::Nothing,
            MaybeAction::Nothing,
            MaybeAction::Nothing,
            MaybeAction::Nothing,
        ],
    ]
}

#[inline(always)]
fn numpad_symbols() -> [[MaybeAction; FULL_COLS]; ROWS] {
    [
        [
            _____,
            _____,
            _____,
            _____,
            _____,
            _____,
            _____,
/* Right */
            _____,
            _____,
            k(Kp7),
            k(Kp8),
            k(Kp9),
            k(KpPlus),
            _____,
        ],
        [
            _____,
            _____,
            _____,
            key('{'),
            key('}'),
            key('$'),
            _____,
/* Right */
            _____,
            key('`'),
            k(Kp4),
            k(Kp5),
            k(Kp6),
            k(KpMinus),
            _____,
        ],
        [
            _____,
            _____,
            _____,
            key('['),
            key(']'),
            key('#'),
            _____,
/* Right */
            _____,
            key('&'),
            k(Kp1),
            k(Kp2),
            k(Kp3),
            k(KpAsterisk),
            _____,
        ],
        [
            _____,
            _____,
            _____,
            key('<'),
            key('>'),
            key('~'),
            XXXXX,
/* Right */
            XXXXX,
            key('_'),
            k(Kp0),
            k(KpDot),
            k(KpEqual),
            k(KpSlash),
            _____,
        ],
        [
            _____,
            _____,
            _____,
            _____,
            _____,
            XXXXX,
            XXXXX,
/* Right */
            XXXXX,
            XXXXX,
            k(Home),
            k(PageDown),
            k(PageUp),
            k(End),
            _____,
        ],
        [
_____, _____, _____, _____, XXXXX, XXXXX, XXXXX, /* Right */
            XXXXX, XXXXX, XXXXX, _____, _____, _____, _____,
        ],
    ]
}

#[inline(always)]
fn shortcuts() -> [[MaybeAction; FULL_COLS]; ROWS] {
    [
        [
_____, _____, _____, _____, _____, _____, _____, /* Right */
            _____, _____, _____, _____, _____, _____, _____,
        ],
        [
_____, _____, _____, _____, _____, _____, _____, /* Right */
            _____, _____, _____, _____, _____, _____, _____,
        ],
        [
_____, _____, _____, _____, _____, _____, _____, /* Right */
            _____, _____, _____, _____, _____, _____, _____,
        ],
        [
_____, _____, _____, _____, _____, _____, XXXXX, /* Right */
            XXXXX, _____, _____, _____, _____, _____, _____,
        ],
        [
            _____,
            _____,
            _____,
            _____,
            _____,
            XXXXX,
            XXXXX,
/* Right */
            XXXXX,
            XXXXX,
            k(Home),
            k(PageDown),
            k(PageUp),
            k(End),
            _____,
        ],
        [
_____, _____, _____, _____, XXXXX, XXXXX, XXXXX, /* Right */
            XXXXX, XXXXX, XXXXX, _____, _____, _____, _____,
        ],
    ]
}

fn _main_layer_obsolete() -> [[MaybeAction; FULL_COLS]; ROWS] {
    [
        [
            k(Esc),
            hold(key('"'), key('1')),
            hold(key('«'), key('2')),
            hold(key('»'), key('3')),
            hold(key('('), key('4')),
            hold(key(')'), key('5')),
            hold(key('$'), key('#')),
/* Right */
            hold(key('%'), key('`')),
            hold(key('@'), key('6')),
            hold(key('+'), key('7')),
            hold(key('-'), key('8')),
            hold(key('/'), key('9')),
            hold(key('*'), key('0')),
            hold(key('='), key('°')),
        ],
        [
            k(Tab),
            hold(key('b'), key('B')),
            hold(key('é'), key('É')),
            hold(key('p'), key('P')),
            hold(key('o'), key('O')),
            hold(key('è'), key('È')),
            k(Undo),
/* Right */
            k(Insert),
            hold(key('^'), key('!')),
            hold(key('v'), key('V')),
            hold(key('d'), key('D')),
            hold(key('l'), key('L')),
            hold(key('j'), key('J')),
            hold(key('z'), key('Z')),
        ],
        [
            m(LeftShift),
            hold(key('a'), key('A')),
            hold(key('u'), key('U')),
            hold(key('i'), key('I')),
            hold(key('e'), key('E')),
            hold(key(','), key(';')),
            hold(k(LeftCtrl + Insert), k(LeftShift + Insert)),
/* Right */
            MaybeAction::Nothing,
            hold(key('c'), key('C')),
            hold(key('t'), key('T')),
            hold(key('s'), key('S')),
            hold(key('r'), key('R')),
            hold(key('n'), key('N')),
            hold(key('m'), key('M')),
        ],
        [
            key('à'),
            hold(key('w'), key('W')),
            hold(key('y'), key('Y')),
            hold(key('x'), key('X')),
            hold(key('.'), key(':')),
            hold(key('k'), key('K')),
            XXXXX,
/* Right */
            XXXXX,
            hold(key('\''), key('?')),
            hold(key('q'), key('Q')),
            hold(key('g'), key('G')),
            hold(key('h'), key('H')),
            hold(key('f'), key('F')),
            hold(key('&'), key('_')),
        ],
        [
            m(LeftAlt),
            key('<'),
            hold(key('à'), key('À')),
            hold(key('{'), key('}')),
            hold(key('['), key(']')),
            XXXXX,
            XXXXX,
/* Right */
            XXXXX,
            XXXXX,
            k(Home),
            k(PageDown),
            k(PageUp),
            k(End),
            _____,
        ],
        [
            k(Space),
            m(LeftGui),
            l(1),
            m(LeftCtrl),
            XXXXX,
            XXXXX,
            XXXXX,
/* Right */
            XXXXX,
            XXXXX,
            XXXXX,
            m(RightGui),
            k(Insert),
            k(E),
            k(T),
        ],
    ]
}
*/
