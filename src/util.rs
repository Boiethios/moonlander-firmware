pub trait ResultExt<T> {
    fn into_ok_(self) -> T;
}

impl<T> ResultExt<T> for Result<T, core::convert::Infallible> {
    #[inline]
    fn into_ok_(self) -> T {
        match self {
            Ok(x) => x,
            Err(e) => match e {},
        }
    }
}

// 100 NOp are something around 1ms
pub fn delay(n: usize) {
    for _ in 0..n {
        cortex_m::asm::nop();
    }
}

/// Returns weither the nth bit is active.
pub fn bit(byte: u8, nth: usize) -> bool {
    (byte >> nth) & 1 == 1
}

/// Sends a debug message through the serial USB. If it fails, it does so silently.
macro_rules! serial_msg {
    ( $fmt_str:literal $( , $param:expr )* ) => {{
        use core::fmt::Write;
        let mut buf = heapless::Vec::new();

        write!(buf, $fmt_str $( , $param )*).ok();

        crate::app::send_debug_message::spawn(buf).ok();
    }};
}
pub(crate) use serial_msg;
