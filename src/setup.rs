use core::mem::MaybeUninit;
use hal::{
    gpio::{gpioa, Gpioa, Input, Pin, U},
    pac::USB,
    prelude::*,
    // UsbBus is the generic type, UsbBusType is the concrete type for this board:
    usb::{self, UsbBus, UsbBusType},
};
use usb_device::bus::UsbBusAllocator;

#[inline]
pub fn usb(
    usb_bus: &'static mut MaybeUninit<UsbBusAllocator<UsbBusType>>,
    usb_periph: USB,
    pa11: Pin<Gpioa, U<11>, Input>,
    pa12: Pin<Gpioa, U<12>, Input>,
    moder: &mut gpioa::MODER,
    otyper: &mut gpioa::OTYPER,
    afrh: &mut gpioa::AFRH,
) -> &'static UsbBusAllocator<UsbBusType> {
    let mut usb_dp = pa12.into_push_pull_output(moder, otyper);

    usb_dp.set_low().ok();
    crate::util::delay(100);

    let usb_dm = pa11.into_af_push_pull(moder, otyper, afrh);
    let usb_dp = usb_dp.into_af_push_pull(moder, otyper, afrh);

    let usb = usb::Peripheral {
        usb: usb_periph,
        pin_dm: usb_dm,
        pin_dp: usb_dp,
    };

    let usb_bus: &'static _ = usb_bus.write(UsbBus::new(usb));

    usb_bus
}
