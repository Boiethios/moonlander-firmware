use core::{arch::asm, ptr};

const BOOTLOAD_OFF: u32 = 0;
const BOOTLOAD_ON: u32 = 0xFEFE_C001;
const FIRMWARE_ADDR: u32 = 0x1FFFD800;

static mut BOOTLOAD_FLAG: u32 = BOOTLOAD_OFF;

/// Resets the device, after setting the flag up for bootloading.
pub fn reset() -> ! {
    unsafe { ptr::write_volatile(core::ptr::addr_of_mut!(BOOTLOAD_FLAG), BOOTLOAD_ON) };

    cortex_m::peripheral::SCB::sys_reset()
}

/// Checks if a bootload is needed after a warm reset, and bootloads if asked for.
pub unsafe fn check() {
    let flag: u32;
    asm!("ldr {0}, [{1}]", out(reg) flag, in(reg) ptr::addr_of!(BOOTLOAD_FLAG));

    if flag == BOOTLOAD_ON {
        jump()
    }
}

/// Does the actual bootload.
unsafe fn jump() -> ! {
    // Reset the flag:
    ptr::write_volatile(ptr::addr_of_mut!(BOOTLOAD_FLAG), BOOTLOAD_OFF);

    // Make the bootloader vector table active:
    (*cortex_m::peripheral::SCB::PTR).vtor.write(FIRMWARE_ADDR);

    // Bootload:
    cortex_m::asm::bootload(FIRMWARE_ADDR as *const u32)
}
